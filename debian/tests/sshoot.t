use strict;
use warnings;

use Test::More;
use Test::Command::Simple;

my $CMD = 'sshoot';

run_ok 2, $CMD;
cmp_ok stdout, 'eq', '', 'bare command, stdout';
like stderr, qr/^$CMD: error: the following arguments are required: ACTION$/m, 'bare command, stderr';

run_ok $CMD, '--help';
like stdout, qr/^usage: $CMD/, 'help, stdout';
cmp_ok stderr, 'eq', '', 'help, stderr';

run_ok $CMD, qw(list);
like stdout, qr/Name\h+Remote host\h+Subnets/, 'list, stdout';
cmp_ok stderr, 'eq', '', 'list, stderr';

done_testing;
